package com.markremi.exercise.laborStats.service;

import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.model.QueryRequest;
import com.google.api.services.bigquery.model.QueryResponse;
import com.google.api.services.bigquery.model.TableCell;
import com.google.api.services.bigquery.model.TableRow;
import com.markremi.exercise.laborStats.model.LaborStat;
import com.markremi.exercise.laborStats.repository.LaborStatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class LaborService {

    @Value("${google.cloud.project.id}")
    private String projectId;
    @Value("${google.cloud.schema}")
    private String schema;
    @Value("${google.cloud.table}")
    private String table;

    @Autowired
    private AuthService authService;
    @Autowired
    private LaborStatRepository repository;

    /**
     * Query Google BigQuery LBS table by year.
     *
     * @param year  Year for LBS filter.
     * @param limit Number of items to be returned.
     * @return List of labor statistics based on year and count.
     * @throws IOException
     */
    public List<LaborStat> queryByYear(String year, int limit) throws IOException {

        String queryString =
                "SELECT * FROM [" + schema + "." + table + "] WHERE year=" + year + " LIMIT " + limit;

        Bigquery bigquery = authService.buildAuthorizedClient();
        QueryResponse query = bigquery
                .jobs()
                .query(projectId, new QueryRequest().setQuery(queryString))
                .execute();

        List<LaborStat> laborStatList = new ArrayList<>();
        String downloadId = UUID.randomUUID().toString();

        // Convert to LaborStats model.
        // There's a better way to do this but making this simple due to time.
        for (TableRow row : query.getRows()) {

            LaborStat laborStat = new LaborStat();
            for (TableCell cell : row.getF()) {
                laborStat.setDownloadId(downloadId);
                laborStat.setSeries_id(row.getF().get(0).getV().toString());
                laborStat.setYear(row.getF().get(1).getV().toString());
                laborStat.setPeriod(row.getF().get(2).getV().toString());
                laborStat.setValue(row.getF().get(3).getV().toString());
                laborStat.setFootnote_codes(row.getF().get(4).getV().toString());
                laborStat.setDate(row.getF().get(5).getV().toString());
                laborStat.setSeries_title(row.getF().get(6).getV().toString());
            }
            laborStatList.add(laborStat);
        }

        // Persist request.
        repository.saveAll(laborStatList);

        return laborStatList;
    }

    /**
     * Download previously executed query by downloadId.
     *
     * @param downloadId Identification for previously executed query.
     * @return List of labor statistics.
     */
    public List<LaborStat> downloadLaborStats(String downloadId) {
        return repository.findByDownloadId(downloadId);
    }
}
