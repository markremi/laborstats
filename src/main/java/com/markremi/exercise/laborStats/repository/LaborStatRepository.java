package com.markremi.exercise.laborStats.repository;

import com.markremi.exercise.laborStats.model.LaborStat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LaborStatRepository extends JpaRepository<LaborStat, Long> {

    List<LaborStat> findByDownloadId(String downloadId);
}
