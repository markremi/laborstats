package com.markremi.exercise.laborStats.controller;

import com.markremi.exercise.laborStats.model.LaborStat;
import com.markremi.exercise.laborStats.service.LaborService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping(value = "/labor")
public class LaborController {

    @Autowired
    private LaborService laborService;

    @PostMapping(value = "/{year}/{count}")
    public @ResponseBody List<LaborStat> getLaborStatsByYear(
            @PathVariable String year, @PathVariable int count) throws IOException {
        return laborService.queryByYear(year, count);
    }

    @PostMapping(value = "/download/{requestUUID}")
    public @ResponseBody List<LaborStat> downloadLaborStats(
            @PathVariable String requestUUID) {
        return laborService.downloadLaborStats(requestUUID);
    }
}
