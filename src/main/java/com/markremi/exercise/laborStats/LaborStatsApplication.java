package com.markremi.exercise.laborStats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LaborStatsApplication {

    public static void main(String[] args) {
        SpringApplication.run(LaborStatsApplication.class, args);
    }
}
