#Cambium Java Exercise

This is what I could get done within a couple of hours of implementation. The `query data` endpoint will reach out to Google's BigQuery and retrieve information based on `year` and `limit`.
* `year`  -  The year for filter.
* `limit` - The number of items to return.
 
## App
The application is a Spring Boot app. `application.properties` file contains the Google Cloud ID, schema, and table information. Persistence is done with an in-memory H2 database. **"Query endpoint" will return labor statistics along with the `downloadId`. This is the ID needed to download data from the "download endpoint".**
 
## Launching the app
### Pre-requisite
Google requires an environment pointing to their JSON credentials. My credentials are located in the `/src/main/resources/` folder:
`GOOGLE_APPLICATION_CREDENTIALS=.../laborStats/src/main/resources/laborStats-f8845838939b.json`

### Run app
` mvn spring-boot:run`

## Endpoints

| Purpose | Uri |
|--|--|
| Query Data | `http://localhost:8080/labor/{year}/{limit}` |
| Download Data | `http://localhost:8080/labor/download/{downloadId}`

## Sample Query usage
`http://localhost:8080/labor/2013/2`
```
[
    {
        "id": 1,
        "downloadId": "9f912faf-e074-4981-9e87-a1a3a320e44b",
        "series_id": "LNU00044225",
        "year": "2013",
        "period": "A01",
        "value": "581.0",
        "footnote_codes": "java.lang.Object@369730f1",
        "date": "2013-12-31",
        "series_title": "(unadj) Population - 50 to 54 years, Asian, women"
    },
    {
        "id": 2,
        "downloadId": "9f912faf-e074-4981-9e87-a1a3a320e44b",
        "series_id": "LNU00050999",
        "year": "2013",
        "period": "A01",
        "value": "1733.0",
        "footnote_codes": "java.lang.Object@369730f1",
        "date": "2013-12-31",
        "series_title": "(Unadj) Population Level - Nonveterans, 55 to 64 years, Asian"
    }
]
```

## Sample download usage
`http://localhost:8080/labor/download/9f912faf-e074-4981-9e87-a1a3a320e44b`

```
[
    {
        "id": 1,
        "downloadId": "9f912faf-e074-4981-9e87-a1a3a320e44b",
        "series_id": "LNU00044225",
        "year": "2013",
        "period": "A01",
        "value": "581.0",
        "footnote_codes": "java.lang.Object@369730f1",
        "date": "2013-12-31",
        "series_title": "(unadj) Population - 50 to 54 years, Asian, women"
    },
    {
        "id": 2,
        "downloadId": "9f912faf-e074-4981-9e87-a1a3a320e44b",
        "series_id": "LNU00050999",
        "year": "2013",
        "period": "A01",
        "value": "1733.0",
        "footnote_codes": "java.lang.Object@369730f1",
        "date": "2013-12-31",
        "series_title": "(Unadj) Population Level - Nonveterans, 55 to 64 years, Asian"
    }
]
```
